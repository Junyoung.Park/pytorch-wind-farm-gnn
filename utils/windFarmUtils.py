import json
import numpy as np
import matplotlib.pyplot as plt


def prepare_floris_json(template_path, xy_layouts, save_path, save=True):
    """
        :param template_path: (str) json base file path
        :param xy_layouts: (list of 2 lists) the first list is for the x-coordinates for the turbines in the farm.
                            The second list is for the y-coordinates for the turbines in the wind farm.
        :param save_path : (str) file path for saving the updated json file
    :return: coordination updated python dictionary
    """

    with open(template_path) as f:
        base = json.load(f)
    try:
        farm_props = base['farm']['properties']
        farm_props.pop('layout_x')
        farm_props.pop('layout_y')
    except KeyError as e:
        print(e)

    base['farm']['properties']['layout_x'] = xy_layouts[0]
    base['farm']['properties']['layout_y'] = xy_layouts[1]

    if save:
        with open(save_path, 'w') as outfile:
            json.dump(base, outfile)

    return base


def valid_layout(x_coords, y_coords, min_dist):
    """
    :param x_coords: (list of floats) list of x coordinates in the grid
    :param y_coords: (list of floats) list of y coordinates in the grid
    :param min_dist: (scalar) minimum distance bet. two arbitrary points to be
                    satisfied by the coordinates.
    :return: validity flag
    """

    # the dumbest way for checking validity of the given layout
    # will be / need to be revised !!

    valid = True
    coords = zip(x_coords, y_coords)
    for c1 in coords:
        for c2 in coords:
            dist = np.linalg.norm(np.array(c1)-np.array(c2))
            if dist <= min_dist:
                valid = False
                return valid
    return valid


def sequential_sampling(x_grid_size, y_grid_size, min_dist, num_turbines):
    """
    :param x_grid_size: (float) X axis grid size
    :param y_grid_size: (float) Y axis grid size
    :param min_dist: (scalar) minimum distance bet. two arbitrary points to be
                    satisfied by the coordinates.
    :param num_turbines: (int) number of sample turbines
    :return:
    """

    # the dumbest way for sampling wind turbine layout
    # Do not guarantee whether given input params are valid for sampling
    # will be / need to be revised

    # Check whether the new coord. is valid against the valid' coords set.
    def valid_sample(x_coords, y_coords, new_x, new_y):
        valid = True
        for x, y in zip(x_coords, y_coords):
            dist = np.linalg.norm(np.array([x, y]) - np.array([new_x, new_y]))
            if dist <= min_dist:
                valid = False
                return valid
        return valid

    turbine_x_coords = []
    turbine_y_coords = []

    while len(turbine_x_coords) < num_turbines:
        x = np.random.uniform(low=0.0, high=x_grid_size)
        y = np.random.uniform(low=0.0, high=y_grid_size)
        if len(turbine_x_coords) == 0:
            pass
        else:
            if not valid_sample(turbine_x_coords, turbine_y_coords, x, y):
                continue
        turbine_x_coords.append(x)
        turbine_y_coords.append(y)
    return turbine_x_coords, turbine_y_coords


def visualize_sampled_points(sampled_x, sampled_y, min_dist,
                             x_limit, y_limit, viz_eps=0.001, index=None):
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.xaxis.grid(True, which='major')
    ax.xaxis.grid(True, which='major')
    ax.yaxis.grid(True, which='major')
    ax.yaxis.grid(True, which='minor')
    ax.set_xlim([-x_limit*viz_eps, x_limit*(1+viz_eps)])
    ax.set_ylim([-y_limit*viz_eps, y_limit+(1+viz_eps)])
    ax.scatter(sampled_x, sampled_y, label='Sampled Turbines')

    if index is None:
        index = range(len(sampled_x))

    for idx, x, y in zip(index, sampled_x, sampled_y):
        circle = plt.Circle((x, y), min_dist/2, color='b', alpha=0.2)
        plt.annotate(idx, (x, y))
        ax.add_artist(circle)
    ax.legend()
    ax.set_title("Sampled Turbine distribution")
    return fig, ax
