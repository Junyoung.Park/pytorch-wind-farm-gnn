import csv
import time
import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.patches import Wedge, Polygon, FancyArrow
from matplotlib.collections import PatchCollection
from matplotlib.lines import Line2D
import matplotlib.patheffects as PathEffects
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
from pprint import pprint
from matplotlib.ticker import NullFormatter

from utils.trainingUtils import Timer

import warnings
warnings.filterwarnings("error")


def pprint_graph(graph):

    print("Node information")
    for n in graph.nodes():
        print('{}:'.format(n))
        pprint(graph.nodes[n])

    print("\n Edge information")
    for e in graph.edges():
        print('{}:'.format(e))
        pprint(graph.edges[e])

    print("\n Global information")
    pprint(graph.graph)


def test_direction(gnn, farm, num_turbines, wind_directions=None, wind_speed=8, fix_farm=True, use_speed=True):
    if not fix_farm:
        farm.sample_wind_farm(num_turbines)

    if wind_directions is None:
        wind_directions = np.arange(0, 360, 1)

    print("Number of turbines : {}".format(num_turbines))
    print("Number of directions : {}".format(len(wind_directions)))

    pred_dict = {}
    target_dict = {}

    pred_dict['wd'] = []
    for i in range(num_turbines):
        pred_dict[i] = []
        target_dict[i] = []

    pred_dict['total'] = []
    target_dict['total'] = []

    xs = []
    ys = []

    for wd in wind_directions:
        pred_dict['wd'].append(wd)
        farm.update_wind_farm_graph(wd, wind_speed, verbose=False)
        test_x, test_y = farm.observe(use_speed=use_speed)
        xs.append(test_x)
        ys.append(test_y)

    with Timer('Wind direction generalization test'):
        pred_y = gnn.predict(xs)

    with Timer('Compute test score'):
        test_score = gnn.test(xs, ys)
    print('test score : {}'.format(test_score))

    for pred_g, tagert_g in zip(pred_y, ys):  # wind directions
        total_power_pred = 0
        total_power_target = 0
        for n in pred_g.nodes:
            power_pred = pred_g.nodes[n]['power']
            power_target = tagert_g.nodes[n]['power']

            pred_dict[n].append(power_pred)
            target_dict[n].append(power_target)
            # total power computation
            total_power_pred += power_pred
            total_power_target += power_target

        pred_dict['total'].append(total_power_pred)
        target_dict['total'].append(total_power_target)

    fig, axes = plt.subplots(num_turbines + 1, 1, figsize=(10, num_turbines * 2.5), sharex=True)
    fig.tight_layout(rect=[0, 0.03, 1, 0.9])
    #fig.tight_layout()

    for i in range(num_turbines):
        axes[i].grid(True)
        axes[i].set_title('Turbine {} power prediction curve'.format(i+1), fontsize=13)
        axes[i].set_xlim(-10.0, 370.0)
        axes[i].set_ylim(0.0, 1.2)
        #target = axes[i].scatter(pred_dict['wd'], target_dict[i], label='TARGET', marker='o', c='b')
        target = axes[i].plot(pred_dict['wd'], target_dict[i], label='Target', linewidth=5.0)
        pred = axes[i].plot(pred_dict['wd'], pred_dict[i], label='Estimation', alpha=0.5, linewidth=5.0)

        if i == 0:
            axes[i].legend()

        if i == round(num_turbines/2):
            axes[i].set_ylabel('normalized powers', fontsize=15)

    axes[-1].grid(True)
    axes[-1].set_ylim(num_turbines/2, num_turbines * 1.2)

    axes[-1].set_title('Total farm power prediction curve', fontsize=13)
    axes[-1].plot(pred_dict['wd'], target_dict['total'], label='TARGET', linewidth=5.0)
    axes[-1].plot(pred_dict['wd'], pred_dict['total'], label='PRED', linewidth=5.0, alpha=0.5,)
    axes[-1].set_xlabel('wind direction ($^\circ$)', fontsize=15)

    #fig.suptitle('Wind Farm Power Estimation Curves', fontsize=16)
    #fig.legend((pred, target), ('estimations', 'targets'))


def test_speed(gnn, farm, num_turbines, wind_speeds=None, wind_directions=None,
               n_col=2, fix_farm=False, normalize_total=True, use_speed=True):
    """
        MAKE SURE The farm has at least one edges for all testing cases!
    """
    if not fix_farm:
        farm.sample_wind_farm(num_turbines)

    if wind_speeds is None:
        wind_speeds = np.linspace(6.0, 15.0, 100)

    if wind_directions is None:
        wind_directions = [0, 30, 60]

    wsmin = min(wind_directions)
    wsmax = max(wind_directions)

    cmap = matplotlib.cm.Accent
    norm = matplotlib.colors.Normalize(vmin=wsmin, vmax=wsmax)

    print("Number of turbines : {}".format(num_turbines))
    print("Number of speeds : {}".format(len(wind_speeds)))

    pred_dict = {}  # pred_dict[turbine_index,wind_dirs] = list
    target_dict = {}  # target_dict[turbine_index,wind_dirs] = list
    total_pred_dict = {}  # total_pred_dict[wind_dirs] = list
    total_target_dict = {}  # total_target_dict[wind_dirs] = list

    for n in range(num_turbines):
        for wd in wind_directions:
            pred_dict[n, wd] = []
            target_dict[n, wd] = []

    # farm-level prediction
    for wd in wind_directions:
        total_pred_dict[wd] = []
        total_target_dict[wd] = []

    # preparing input graphs and target graphs
    xs = {}  # xs[wind_dirs,wind_speeds] = input graph when wind_dir, wind_speed is given
    ys = {}  # ys[wind_dirs,wind_speeds] = target graph when wind_dir, wind_speed is given

    for wd in wind_directions:
        for ws in wind_speeds:
            xs[wd, ws] = []
            ys[wd, ws] = []

    for wd in wind_directions:
        for ws in wind_speeds:
            farm.update_wind_farm_graph(wd, ws, verbose=False)
            test_x, test_y = farm.observe(use_speed=use_speed)
            xs[wd, ws] = test_x
            ys[wd, ws] = test_y

    # predict w.r.t wind directions
    for wd in wind_directions:
        _x = []
        _y = []
        for ws in wind_speeds:
            _x.append(xs[wd, ws])
            _y.append(ys[wd, ws])

        with Timer('[wind direction = {}]'.format(wd)):
            pred_wd = gnn.predict(_x)
            test_score_wd = gnn.test(_x, _y)

        print('test score : {}'.format(test_score_wd))

        for pred_g_w, target_g_w in zip(pred_wd, _y):  # iterating over wind speed
            total_power_pred_wd = 0
            total_power_target_wd = 0
            for n in pred_g_w.nodes:
                power_pred_n_wd = pred_g_w.nodes[n]['power']
                power_pred_n_wd = float(power_pred_n_wd)
                power_taget_n_wd = target_g_w.nodes[n]['power']

                pred_dict[n, wd].append(power_pred_n_wd)
                target_dict[n, wd].append(power_taget_n_wd)
                total_power_pred_wd += power_pred_n_wd
                total_power_target_wd += power_taget_n_wd
            if normalize_total:
                total_power_pred_wd = total_power_pred_wd / num_turbines
                total_power_target_wd = total_power_target_wd / num_turbines
            total_pred_dict[wd].append(total_power_pred_wd)
            total_target_dict[wd].append(total_power_target_wd)
    
    ncols = n_col
    quotient, remainder = np.divmod(num_turbines+1, ncols)
    if remainder > 0:
        quotient += 1
    nrows = quotient

    fig, axes = plt.subplots(nrows, ncols, figsize=(ncols * 5, nrows * 5))
    #fig.tight_layout(rect=[1.1, 0, 0.8, 0.9])
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.15)

    for n, ax in enumerate(axes.reshape(-1)[:num_turbines]):
        ax.grid(True)
        ax.set_ylim(0.0, 1.2)
        ax.set_title('Turbine {} Power Curve'.format(n+1), fontsize=15, pad=10.0)

        for wd in wind_directions:
            r, g, b, a = cmap(norm(wd))
            c = np.array([r, g, b, a]).reshape(1, -1)
            pred = ax.plot(wind_speeds, pred_dict[n, wd],
                           label='Estimation at {}$^\circ$'.format(wd), color=np.squeeze(c),
                           linewidth=5.0, alpha=0.5)
            target = ax.scatter(wind_speeds, target_dict[n, wd],
                                label='Target at {}$^\circ$'.format(wd), marker='*', c=c)
        # if n == 0:
        #     ax.legend()
        #     ax.set_ylabel('normalized power', fontsize=15)
        #     ax.set_xlabel('wind speed (m/s)', fontsize=15)

    axes[-1][0].legend()
    axes[-1][0].set_ylabel('normalized power', fontsize=15)
    axes[-1][0].set_xlabel('wind speed (m/s)', fontsize=15)

    # Turn-off blank subplots
    for n, ax in enumerate(axes.reshape(-1)[num_turbines:-1]):
        ax.axis('off')

    # Total power predictions
    axes[-1, -1].grid(True)
    axes[-1, -1].set_ylim(0.0, 1.2)
    axes[-1, -1].set_title('Total Power curve', fontsize=15, pad=10.0)

    for wd in wind_directions:
        r, g, b, a = cmap(norm(wd))
        c = np.array([r, g, b, a]).reshape(1, -1)
        pred = axes[-1, -1].plot(wind_speeds, total_pred_dict[wd],
                                 label='Estimation', color=np.squeeze(c),
                                 linewidth=5.0, alpha=0.5)
        target = axes[-1, -1].scatter(wind_speeds, total_target_dict[wd], label='Target', marker='*', c=c)

    #fig.suptitle('Wind Farm Power Prediction Curves', fontsize=16)
    #plt.legend(handles=[pred, target])


def test_speed_direction(gnn, farm, num_turbines, save_path, wind_directions=None, wind_speed=None, num_samples=20,
                         use_speed=True):
    if wind_directions is None:
        wind_directions = np.arange(0, 360, 1)

    if wind_speed is None:
        wind_speed = np.linspace(8.0, 15.0, 100)

    means = []
    stds = []

    total_iters = len(wind_directions) * len(wind_speed)
    iters = 0
    for wd in wind_directions:
        temp_means = []
        temp_stds = []
        for ws in wind_speed:
            xs = []
            ys = []
            for _ in range(num_samples):
                try:
                    farm.sample_wind_farm(num_turbines)
                    farm.update_wind_farm_graph(wd, ws, verbose=False)
                    test_x, test_y = farm.observe(use_speed=use_speed)
                    xs.append(test_x)
                    ys.append(test_y)
                except RuntimeWarning:  # when sqrt(negative vals)
                    pass
                except ValueError:  # computing wake has problem
                    pass

            test_scores = []

            print(len(xs))

            for x, y in zip(xs, ys):
                test_score = gnn.test([x], [y])
                test_scores.append(test_score)

            mean_score = np.mean(test_scores)
            std_score = np.std(test_scores)
            temp_means.append(mean_score)
            temp_stds.append(std_score)

            iters += 1
            if iters % 10 == 0:
                print('Now computing [{}/{}] pairs'.format(iters, total_iters))

        means.append(temp_means)
        stds.append(temp_stds)

    means = np.stack(means)
    stds = np.stack(stds)

    with open(save_path + '_mean.csv', 'w') as csv_file:
        fieldnames = ['wind_direction'] + (list(wind_speed))
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for l, wd in zip(means, wind_directions):
            row = {}
            for e, ws in zip(l, wind_speed):
                row['wind_direction'] = wd
                row[ws] = e
            writer.writerow(row)

    with open(save_path + '_std.csv', 'w') as csv_file:
        fieldnames = ['wind_direction'] + (list(wind_speed))
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for l, wd in zip(stds, wind_directions):
            row = {}
            for e, ws in zip(l, wind_speed):
                row['wind_direction'] = wd
                row[ws] = e
            writer.writerow(row)

    return means, stds


def test_turbine_numbers(gnn, farm, num_turbines,
                         min_wd=0, max_wd=360,
                         min_ws=8.0, max_ws=15.0,
                         num_sampling=20, use_speed=True):
    
    # I know the following code is the uglest ever
    # but works descently. Don't blame my baby too much :p
    
    means = []
    stds = []
    for i, num_turbine in enumerate(num_turbines):
        print("[{}/{}] Comutping {} case ".format(i, len(num_turbines), num_turbine))
        xs = []
        ys = []
        inner_loop_counter = 0
        while inner_loop_counter < num_sampling: # Persisting to sample the given number of samplings
            # The sampling procedure do not gaurantee to end.

            # To prevent 'forever sampling' situation, I put timeout inside layout sampling
            try: 
                # A hacky way for preventing wind farm graph update inside of 'sample_wind_farm'
                farm.wind_speed = None 
                farm.wind_direction = None
                # Sampling layouts
                farm.sample_wind_farm(num_turbine)
                while True:
                    # FLORIS give you an numerical error when the inputs; layout, wind speed, wind direciton
                    # is not proper. To solve the issue, I put additional while loop.
                    try:
                        wd = np.random.uniform(low=min_wd, high=max_wd, size=1)[0]
                        ws = np.random.uniform(low=min_ws, high=max_ws, size=1)[0]
                        farm.update_wind_farm_graph(wd, ws, verbose=False)
                        test_x, test_y = farm.observe(use_speed=use_speed)
                        xs.append(test_x)
                        ys.append(test_y)
                        inner_loop_counter += 1
                        break
                    except RuntimeWarning as rw:
                        pass
            except TimeoutError as te:
                pass
        
        test_scores = []
        for x, y in zip(xs, ys):
            test_score = gnn.test([x], [y])
            test_scores.append(test_score)
        
        mean_score = np.mean(test_scores)
        std_score = np.std(test_scores)
        means.append(mean_score)
        stds.append(std_score)
        
    return means, stds


def test_compuation_time(gnn, farm, num_turbines,
                         min_wd=0, max_wd=360,
                         min_ws=8.0, max_ws=15.0,
                         num_sampling=20, use_speed=True):
    means = []
    stds = []

    means = []
    stds = []
    for i, num_turbine in enumerate(num_turbines):
        print("[{}/{}] Comutping {} case ".format(i, len(num_turbines), num_turbine))
        xs = []
        ys = []
        inner_loop_counter = 0
        while inner_loop_counter < num_sampling:  # Persisting to sample the given number of samplings
            # The sampling procedure do not gaurantee to end.
            # To prevent 'forever sampling' situation, I put timeout inside layout sampling
            try:
                # A hacky way for preventing wind farm graph update inside of 'sample_wind_farm'
                farm.wind_speed = None
                farm.wind_direction = None
                # Sampling layouts
                farm.sample_wind_farm(num_turbine)
                while True:
                    # FLORIS give you an numerical error when the inputs; layout, wind speed, wind direciton
                    # is not proper. To solve the issue, I put additional while loop.
                    try:
                        wd = np.random.uniform(low=min_wd, high=max_wd, size=1)[0]
                        ws = np.random.uniform(low=min_ws, high=max_ws, size=1)[0]
                        farm.update_wind_farm_graph(wd, ws, verbose=False)
                        test_x, test_y = farm.observe(use_speed=use_speed)
                        xs.append(test_x)
                        ys.append(test_y)
                        inner_loop_counter += 1
                        break
                    except RuntimeWarning as rw:
                        pass
            except TimeoutError as te:
                pass

        test_times = []
        for x, y in zip(xs, ys):
            start_time = time.time()
            _ = gnn.test([x], [y])
            end_time = time.time()
            delta = end_time - start_time
            test_times.append(delta)

        mean_time = np.mean(test_times)
        std_time = np.std(test_times)
        means.append(mean_time)
        stds.append(std_time)

    return means, stds


def rotate(angle, vector):
    # Clockwise rotation matrix
    rad = np.deg2rad(angle)
    mat = np.array([[np.cos(rad), np.sin(rad)], [-np.sin(rad), np.cos(rad)]])
    return mat.dot(vector)


def attatch_spider_chart(ax, center, angles,
                         unit_length=1.0,
                         annote=True,
                         annote_margin=1.25,
                         fontsize=5,
                         annotation_adjust=0.25):
    # Right West = 0 Degree
    # Right North = 90 Degree

    unit_vec = np.array([-unit_length, 0])
    rotateds = []
    for angle in angles:
        rotateds.append(rotate(angle, unit_vec))

    if annote:
        for angle in angles[0::3]:
            annote_x, annote_y = rotate(angle, np.array([-unit_length * annote_margin, 0]))
            annote_x -= unit_length * annotation_adjust
            annote_y -= unit_length * annotation_adjust
            annote_x += center[0]
            annote_y += center[1]
            ax.annotate("{} $^\circ$".format(angle),
                        (annote_x, annote_y),
                        horizontalalignment='left',
                        verticalalignment='bottom',
                        fontsize=fontsize)

    # Translation
    for rotated in rotateds:
        rotated[0] = rotated[0] + center[0]
        rotated[1] = rotated[1] + center[1]

    coords_2d = np.stack(rotateds)

    # Prepare outter polygon
    polygon = Polygon(coords_2d, closed=True, facecolor='white', edgecolor='grey')
    polygon_patch = [polygon]

    # Prepare guide inner circles
    sub_levels = [0.8, 0.6, 0.4, 0.2]  # Order matters!
    circle_patch = []
    for sub_level in sub_levels:
        circle = Wedge(center, sub_level * unit_length, 0, 360, fill=False, edgecolor='grey')
        circle_patch.append(circle)

    # Prepare lines for indicating angles
    line_patch = []
    for rotated in rotateds:
        x = (center[0], rotated[0])
        y = (center[1], rotated[1])
        line = Line2D(x, y, c='grey', alpha=0.2)
        line_patch.append(line)

    p = PatchCollection(polygon_patch)
    p.set_facecolor('white')
    p.set_edgecolor('grey')
    ax.add_collection(p)

    for line in line_patch:
        ax.add_line(line)

    c = PatchCollection(circle_patch)
    c.set_facecolor('white')
    c.set_edgecolor('grey')
    ax.add_collection(c)


def draw_values(ax, center, angles, preds, targets, unit_length=1.0, line_width=1.0):
    unit_vec = np.array([-unit_length, 0])
    rotateds = []
    for angle in angles:
        rotateds.append(rotate(angle, unit_vec))

    coords_2d = np.stack(rotateds)

    # Prepare prediction polygon
    pred_x = preds * coords_2d[:, 0] + center[0]
    pred_y = preds * coords_2d[:, 1] + center[1]
    pred_xy = np.vstack((pred_x, pred_y)).T

    pred_polygon = Polygon(pred_xy, closed=True, label='estimations')
    pred_patch = [pred_polygon]

    # Prepare target polygon
    target_x = targets * coords_2d[:, 0] + center[0]
    target_y = targets * coords_2d[:, 1] + center[1]
    target_xy = np.vstack((target_x, target_y)).T

    target_polygon = Polygon(target_xy, closed=True, label='targets')
    target_patch = [target_polygon]

    pr = PatchCollection(pred_patch, alpha=0.8, zorder=2.0)

    pr.set_linewidth(line_width)
    pr.set_facecolor('powderblue')
    pr.set_edgecolor('royalblue')
    ax.add_collection(pr)

    t = PatchCollection(target_patch, alpha=1.0, zorder=1.0)
    t.set_linewidth(line_width)
    t.set_facecolor('mistyrose')
    t.set_edgecolor('salmon')
    ax.add_collection(t)

    patchs = dict()
    patchs['estimated'] = pred_polygon
    patchs['target'] = target_polygon
    return patchs


def draw_wind_direction_polygon_chart(gnn, farm, num_turbines,
                                      wind_directions=None,
                                      wind_speed=10.0,
                                      unit_length=10.0,
                                      fix_farm=True,
                                      use_speed=True,
                                      annote_angles=True):
    if not fix_farm:
        farm.sample_wind_farm(num_turbines)

    if wind_directions is None:
        wind_directions = np.arange(0, 360, 30)

    print("Number of turbines : {}".format(num_turbines))
    print("Number of directions : {}".format(len(wind_directions)))

    pred_dict = {}
    target_dict = {}

    pred_dict['wd'] = []
    for i in range(num_turbines):
        pred_dict[i] = []
        target_dict[i] = []

    pred_dict['total'] = []
    target_dict['total'] = []

    xs = []
    ys = []

    for wd in wind_directions:
        pred_dict['wd'].append(wd)
        farm.update_wind_farm_graph(wd, wind_speed, verbose=False)
        test_x, test_y = farm.observe(use_speed=use_speed)
        xs.append(test_x)
        ys.append(test_y)

    with Timer('Wind direction generalization test'):
        pred_y = gnn.predict(xs)

    with Timer('Compute test score'):
        test_score = gnn.test(xs, ys)
    print('test score : {}'.format(test_score))

    for pred_g, tagert_g in zip(pred_y, ys):  # wind directions
        total_power_pred = 0
        total_power_target = 0
        for n in pred_g.nodes:
            power_pred = pred_g.nodes[n]['power']
            power_target = tagert_g.nodes[n]['power']

            pred_dict[n].append(power_pred)
            target_dict[n].append(power_target)
            # total power computation
            total_power_pred += power_pred
            total_power_target += power_target

        pred_dict['total'].append(total_power_pred)
        target_dict['total'].append(total_power_target)

    fig, ax = plt.subplots(figsize=(10, 10), dpi=300)
    ax.set_xlim(-farm.x_grid_size * 0.1, farm.x_grid_size * 1.1)
    ax.set_ylim(-farm.y_grid_size * 0.1, farm.y_grid_size * 1.1)

    viz_graph = farm.observe_viz()  # Visualization purpose graph

    # find the top-left most turbine for visualization

    left_x = 0
    top_y = 0

    for i in viz_graph.nodes:
        _x, _y = viz_graph.nodes[i]['coords']
        if _x <= left_x:
            left_x = _x
        if _y >= top_y:
            top_y = _y

    top_left_turbine_index = farm.coords2index[(left_x, top_y)]

    for i in viz_graph.nodes:  # node index
        coord = viz_graph.nodes[i]['coords']
        center = (coord[0], coord[1])
        angles = pred_dict['wd']
        preds = np.array(pred_dict[i]).flatten()
        targets = np.array(target_dict[i]).flatten()

        if annote_angles:
            if top_left_turbine_index == i:
                _annote = True
            else:
                _annote = False

        attatch_spider_chart(ax=ax,
                             center=center,
                             angles=angles,
                             unit_length=unit_length,
                             annote=_annote)

        patchs = draw_values(ax=ax,
                             center=center,
                             angles=angles,
                             preds=preds,
                             targets=targets,
                             unit_length=unit_length)

    ax.set_xlabel("Wind farm x size")
    ax.set_ylabel("Wind farm y size")

    handles, labels = ax.get_legend_handles_labels()

    # Set legend symbol colors manually
    patchs['estimated'].set_facecolor('powderblue')
    patchs['estimated'].set_edgecolor('royalblue')

    patchs['target'].set_facecolor('lightgrey')
    patchs['target'].set_edgecolor('dimgrey')

    handles += [patchs['estimated'], patchs['target']]
    labels += ['estimation', 'target']
    ax.legend(handles, labels)

    ret_dict = dict()
    ret_dict['fig'] = fig
    ret_dict['ax'] = ax
    ret_dict['patchs'] = patchs
    return ret_dict


def draw_wind_direction_polygon_charts(pib, dib, farm, num_turbines,
                                       wind_directions=None,
                                       wind_speeds=None,
                                       unit_length=230.0,
                                       fix_farm=True,
                                       use_speed=True,
                                       annote_angles=True,
                                       title_size=15,
                                       label_size=12,
                                       tick_size=12,
                                       annote_size=10,
                                       legend_size=12,
                                       polygon_line_width=10,
                                       dpi=300,
                                       figsize_multiplier=10,
                                       viz_eps=0.1,
                                       show_title=False):

    if not fix_farm:
        farm.sample_wind_farm(num_turbines)

    if wind_speeds is None:
        wind_speeds = [5.0, 10.0, 15.0]

    if wind_directions is None:
        wind_directions = np.arange(0, 360, 30)

    n_rows = len(wind_speeds)
    fig, axes = plt.subplots(nrows=n_rows,
                             ncols=2,
                             figsize=(2*figsize_multiplier, n_rows*figsize_multiplier),
                             dpi=dpi)
    fig.tight_layout()

    axes = axes.reshape(-1, 2)
    for i, wind_speed in enumerate(wind_speeds):  # row
        for j in range(2):  # column
            ax = axes[i][j]
            ax.grid(True)

            ax.set_xlim(-farm.x_grid_size * viz_eps, farm.x_grid_size * (1+viz_eps))
            ax.set_ylim(-farm.y_grid_size * viz_eps, farm.y_grid_size * (1+viz_eps))

            pred_dict = {}
            target_dict = {}

            pred_dict['wd'] = []
            for n in range(num_turbines):
                pred_dict[n] = []
                target_dict[n] = []

            pred_dict['total'] = []
            target_dict['total'] = []

            xs = []
            ys = []

            for wd in wind_directions:
                pred_dict['wd'].append(wd)
                farm.update_wind_farm_graph(wd, wind_speed, verbose=False)
                test_x, test_y = farm.observe(use_speed=use_speed)
                xs.append(test_x)
                ys.append(test_y)

            if j == 0:  # left column
                gnn = pib
                _model = 'PIB'
            else:  # right column
                gnn = dib
                _model = 'DIB'

            log_sentence = 'test {} at ws={}'.format(_model, wind_speed)
            with Timer(log_sentence):
                pred_y = gnn.predict(xs)

            with Timer('Compute test score'):
                test_score = gnn.test(xs, ys)
            print('test score : {}'.format(test_score))

            for pred_g, tagert_g in zip(pred_y, ys):  # wind directions
                total_power_pred = 0
                total_power_target = 0
                for n in pred_g.nodes:
                    power_pred = pred_g.nodes[n]['power']
                    power_target = tagert_g.nodes[n]['power']

                    pred_dict[n].append(power_pred)
                    target_dict[n].append(power_target)
                    # total power computation
                    total_power_pred += power_pred
                    total_power_target += power_target

                pred_dict['total'].append(total_power_pred)
                target_dict['total'].append(total_power_target)

            viz_graph = farm.observe_viz()  # Visualization purpose graph

            # find the top-left most turbine for visualization
            left_x = 0
            top_y = 0

            for _i in viz_graph.nodes:
                _x, _y = viz_graph.nodes[_i]['coords']
                if _x <= left_x:
                    left_x = _x
                if _y >= top_y:
                    top_y = _y

            top_left_turbine_index = farm.coords2index[(left_x, top_y)]

            for __i in viz_graph.nodes:  # node index
                coord = viz_graph.nodes[__i]['coords']
                center = (coord[0], coord[1])
                angles = pred_dict['wd']
                preds = np.array(pred_dict[__i]).flatten()
                targets = np.array(target_dict[__i]).flatten()

                if annote_angles:
                    if top_left_turbine_index == __i:
                        _annote = True
                    else:
                        _annote = False

                attatch_spider_chart(ax=ax,
                                     center=center,
                                     angles=angles,
                                     unit_length=unit_length,
                                     annote=_annote,
                                     fontsize=annote_size)

                patchs = draw_values(ax=ax,
                                     center=center,
                                     angles=angles,
                                     preds=preds,
                                     targets=targets,
                                     unit_length=unit_length,
                                     line_width=polygon_line_width)

            top_cond = (i == 0)
            bottom_cond = (i == axes.shape[0]-1)
            left_cond = (j == 0)
            right_cond = (j == 1)

            ax.annotate("Wind speed : {} m/s".format(wind_speed),
                        (farm.x_grid_size * 0.25, (farm.y_grid_size * (1+viz_eps)) * 0.925),
                        fontsize=label_size)

            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)

            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)

            if not left_cond:
                ax.yaxis.set_major_formatter(NullFormatter())

            if not bottom_cond:
                ax.xaxis.set_major_formatter(NullFormatter())

            if top_cond and left_cond:
                if show_title:
                    ax.set_title("PGNN", fontsize=title_size)

                handles, labels = ax.get_legend_handles_labels()
                # Set legend symbol colors manually
                patchs['estimated'].set_facecolor('powderblue')
                patchs['estimated'].set_edgecolor('royalblue')

                patchs['target'].set_facecolor('mistyrose')
                patchs['target'].set_edgecolor('salmon')

                handles += [patchs['estimated'], patchs['target']]
                labels += ['estimation', 'target']
                ax.legend(handles, labels, prop={'size': legend_size}, loc=2)

            if top_cond and right_cond:
                if show_title:
                    ax.set_title("DGNN", fontsize=title_size)

            if bottom_cond and left_cond:
                ax.set_xlabel("Wind farm x size", fontsize=label_size)
                ax.set_ylabel("Wind farm y size", fontsize=label_size)

    return fig


def generalization_over_direction_speed(gnn, farm,
                                        fix_directions=None,
                                        fix_speed=None,
                                        wind_directions=None,
                                        wind_speeds=None,
                                        fix_farm=True,
                                        use_speed=True,
                                        normalize_total=False,
                                        loss='RMSE'):
    if not fix_farm:
        farm.sample_wind_farm(farm.num_turbine)

    if fix_directions is None:
        fix_directions = [60, 120, 180]

    if fix_speed is None:
        fix_speed = 8.0

    if wind_directions is None:
        wind_directions = np.arange(0, 360, 1)

    if wind_speeds is None:
        wind_speeds = np.linspace(6.0, 15.0, 10)

    num_turbines = farm.num_turbine

    print("Number of turbines : {}".format(num_turbines))
    print("Number of directions : {}".format(len(wind_directions)))
    print("Fixed speed : {}".format(fix_speed))
    print("Fixed directions : {}".format(fix_directions))

    # Prepare data points for Wind direction generalization tests

    wd_pred_dict = {}
    wd_target_dict = {}

    wd_pred_dict['wd'] = []
    for i in range(num_turbines):
        wd_pred_dict[i] = []
        wd_target_dict[i] = []

    wd_pred_dict['total'] = []
    wd_target_dict['total'] = []

    xs = []
    ys = []

    for wd in wind_directions:
        wd_pred_dict['wd'].append(wd)
        farm.update_wind_farm_graph(wd, fix_speed, verbose=False)
        test_x, test_y = farm.observe(use_speed=use_speed)
        xs.append(test_x)
        ys.append(test_y)

    with Timer('[Wind Direction] Estimate powers'):
        pred_y = gnn.predict(xs)

    with Timer('[Wind Direction] Compute error'):
        test_score = gnn.test(xs, ys, loss=loss)
    print('[Wind Direction] test score : {}'.format(test_score))

    for pred_g, tagert_g in zip(pred_y, ys):  # wind directions
        total_power_pred = 0
        total_power_target = 0
        for n in pred_g.nodes:
            power_pred = pred_g.nodes[n]['power']
            power_target = tagert_g.nodes[n]['power']

            wd_pred_dict[n].append(power_pred)
            wd_target_dict[n].append(power_target)
            # total power computation
            total_power_pred += power_pred
            total_power_target += power_target
        if normalize_total:
            total_power_pred = total_power_pred / num_turbines
            total_power_target = total_power_target / num_turbines

        wd_pred_dict['total'].append(total_power_pred)
        wd_target_dict['total'].append(total_power_target)

    # Prepare data points for Wind speed generalization tests

    ws_pred_dict = {}  # pred_dict[turbine_index,wind_dirs] = list
    ws_target_dict = {}  # target_dict[turbine_index,wind_dirs] = list
    ws_total_pred_dict = {}  # total_pred_dict[wind_dirs] = list
    ws_total_target_dict = {}  # total_target_dict[wind_dirs] = list

    for n in range(num_turbines):
        for wd in fix_directions:
            ws_pred_dict[n, wd] = []
            ws_target_dict[n, wd] = []

    # farm-level prediction
    for wd in fix_directions:
        ws_total_pred_dict[wd] = []
        ws_total_target_dict[wd] = []

    # preparing input graphs and target graphs
    xs = {}  # xs[wind_dirs,wind_speeds] = input graph when wind_dir, wind_speed is given
    ys = {}  # ys[wind_dirs,wind_speeds] = target graph when wind_dir, wind_speed is given

    for wd in fix_directions:
        for ws in wind_speeds:
            xs[wd, ws] = []
            ys[wd, ws] = []

    for wd in fix_directions:
        for ws in wind_speeds:
            farm.update_wind_farm_graph(wd, ws, verbose=False)
            test_x, test_y = farm.observe(use_speed=use_speed)
            xs[wd, ws] = test_x
            ys[wd, ws] = test_y

    # predict w.r.t wind directions
    for wd in fix_directions:
        _x = []
        _y = []
        for ws in wind_speeds:
            _x.append(xs[wd, ws])
            _y.append(ys[wd, ws])

        with Timer('[wind direction = {}]'.format(wd)):
            pred_wd = gnn.predict(_x)
            test_score_wd = gnn.test(_x, _y)

        print('test score : {}'.format(test_score_wd))

        for pred_g_w, target_g_w in zip(pred_wd, _y):  # iterating over wind speed
            total_power_pred_wd = 0
            total_power_target_wd = 0
            for n in pred_g_w.nodes:
                power_pred_n_wd = pred_g_w.nodes[n]['power']
                power_pred_n_wd = float(power_pred_n_wd)
                power_taget_n_wd = target_g_w.nodes[n]['power']

                ws_pred_dict[n, wd].append(power_pred_n_wd)
                ws_target_dict[n, wd].append(power_taget_n_wd)
                total_power_pred_wd += power_pred_n_wd
                total_power_target_wd += power_taget_n_wd
            if normalize_total:
                total_power_pred_wd = total_power_pred_wd / num_turbines
                total_power_target_wd = total_power_target_wd / num_turbines
            ws_total_pred_dict[wd].append(total_power_pred_wd)
            ws_total_target_dict[wd].append(total_power_target_wd)

    # Draw figures
    mutiplier = 3.0

    wsmin = min(fix_directions)
    wsmax = max(fix_directions)

    cmap = matplotlib.cm.Accent
    norm = matplotlib.colors.Normalize(vmin=wsmin, vmax=wsmax)

    fig = plt.figure(figsize=(num_turbines * mutiplier, num_turbines * mutiplier),
                     dpi=200)
    rows = gridspec.GridSpec(num_turbines + 1, 1, hspace=0.5)  # Num. turbine + Total

    for i in range(num_turbines):
        inner = gridspec.GridSpecFromSubplotSpec(1, 2,
                                                 width_ratios=[2, 1],
                                                 subplot_spec=rows[i],
                                                 wspace=0.1,
                                                 hspace=0.1)

        for j in range(2):
            ax = plt.Subplot(fig, inner[j])

            if j == 0:  # Wind direction curve
                ax.set_title('Turbine {} Power Curve'.format(i + 1),
                             fontsize=17,
                             position=(0.8, 1.05))
                ax.set_xlim(-10.0, 370.0)
                ax.set_ylim(0.0, 1.2)
                ax.plot(wd_pred_dict['wd'], wd_target_dict[i], label='Target', linewidth=5.0)
                ax.plot(wd_pred_dict['wd'], wd_pred_dict[i], label='Estimation', alpha=0.5, linewidth=5.0)
                ax.grid(True)
                ax.set_xticks(np.arange(0.0, 380.0, 20))
                ax.set_yticks(np.arange(0.0, 1.2, 0.2))

                if i == 0:
                    ax.legend(loc=4)
                    # ax.legend(bbox_to_anchor=(0., 1.10, .25, 1.), loc=3,
                    #           ncol=2, mode="expand", borderaxespad=0.)

                if i == round(num_turbines / 2):
                    ax.set_ylabel('normalized powers', fontsize=15)
            else:
                ax.set_ylim(0.0, 1.2)
                ax.set_xlim(5.5, 15.5)
                for wd in fix_directions:
                    r, g, b, a = cmap(norm(wd))
                    c = np.array([r, g, b, a]).reshape(1, -1)
                    pred = ax.plot(wind_speeds,
                                   ws_pred_dict[i, wd],
                                   label='Estimation at {} $^\circ$'.format(wd),
                                   color=np.squeeze(c),
                                   linewidth=5.0,
                                   alpha=0.5)
                    target = ax.scatter(wind_speeds,
                                        ws_target_dict[i, wd],
                                        label='Target at {} $^\circ$'.format(wd),
                                        marker='*',
                                        c=c)
                ax.grid(True)
                ax.set_xticks(np.arange(6.0, 16.0, 1.0))
                ax.set_yticks(np.arange(0.0, 1.2, 0.2))
                if i == 0:
                    ax.legend(ncol=2)
            #                     ax.legend(bbox_to_anchor=(0., 1.10, 1., 1.), loc=3,
            #                               ncol=2, mode="expand", borderaxespad=0.)

            fig.add_subplot(ax)

    # The last row

    inner = gridspec.GridSpecFromSubplotSpec(1, 2,
                                             width_ratios=[2, 1],
                                             subplot_spec=rows[num_turbines],
                                             wspace=0.1,
                                             hspace=0.1)

    last_row_axes = []
    for j in range(2):
        if j == 0:  # Wind direction curve
            ax = plt.Subplot(fig, inner[j])
            title = '(Normalized) Sum of Power Curves' if normalize_total else 'Sum of Power Curves'
            ax.set_title(title,
                         fontsize=17,
                         position=(0.8, 1.05))

            ax.plot(wd_pred_dict['wd'], wd_target_dict['total'], label='Target', linewidth=5.0)
            ax.plot(wd_pred_dict['wd'], wd_pred_dict['total'], label='Estimation', alpha=0.5, linewidth=5.0)
            ax.grid(True)

            ax.set_xlim(-10.0, 370.0)
            ax.set_xticks(np.arange(0.0, 380.0, 20))

            ax.set_xlabel("wind direction ($^\circ$)", fontsize=12)
        else:
            ax = plt.Subplot(fig, inner[j], sharey=last_row_axes[0])
            ax.set_xlim(5.5, 15.5)
            for wd in fix_directions:
                r, g, b, a = cmap(norm(wd))
                c = np.array([r, g, b, a]).reshape(1, -1)
                pred = ax.plot(wind_speeds,
                               ws_total_pred_dict[wd],
                               label='Estimation at {} $^\circ$'.format(wd),
                               color=np.squeeze(c),
                               linewidth=5.0,
                               alpha=0.5)
                target = ax.scatter(wind_speeds,
                                    ws_total_target_dict[wd],
                                    label='Target at {} $^\circ$'.format(wd),
                                    marker='*',
                                    c=c)
            ax.grid(True)
            ax.set_xticks(np.arange(6.0, 16.0, 1.0))
            if i == 0:
                ax.legend()

            ax.set_xlabel('wind speed (m/s)', fontsize=12)

        if normalize_total:
            ax.set_ylim(0.0, 1.2)
            ax.set_yticks(np.arange(0.0, 1.2, 0.2))

        fig.add_subplot(ax)
        last_row_axes.append(ax)

    return fig


def draw_heat_map(mean, std, wind_directions, wind_speeds,
                  dpi=300, show_text=True,
                  fig_title=r'$e(\theta, s)$',
                  fig_title_font_size=25,
                  fig_label_size=20,
                  fig_tick_size=15):

    """
    :param mean: mean is a 2d numpy array. the rows, columns indicate wind directions and wind speeds respectively.
    :param std: similar to mean
    :param wind_directions: x tick labels
    :param wind_speeds: y tick labels
    :param dpi: figure dpi
    :param show_text: Boolean for denoting whether displaying annotations or not.
    :param fig_title_font_size: float
    :param fig_label_size: float
    :param fig_tick_size: float
    :return: figure object
    """

    fig, ax = plt.subplots(figsize=(len(wind_speeds), len(wind_directions)), dpi=dpi)
    im = ax.imshow(mean.T, cmap='YlGn')  # [wind speeds x wind directions]
    ax.set_title(fig_title, fontsize=fig_title_font_size, position=(0.5, 1.03))

    ax.set_xticks(np.arange(len(wind_directions)))
    ax.set_xticklabels(wind_directions, fontsize=fig_tick_size)

    ax.set_yticks(np.arange(len(wind_speeds)))
    ax.set_yticklabels(wind_speeds, fontsize=fig_tick_size)

    ax.set_xlabel("wind direction ($^\circ$)", fontsize=fig_label_size)
    ax.set_ylabel("wind speed (m/s)", fontsize=fig_label_size)

    if show_text:
        for i in range(len(wind_directions)):
            for j in range(len(wind_speeds)):
                text = ax.text(i, j, "{:.3f} \u00B1 {:.3f}".format(mean[i, j], std[i, j]),
                               ha="center", va="center", color="k", fontsize=12)

    min_error = np.min(mean)
    max_error = np.max(mean)

    num_ticks = 10
    increment = (max_error - min_error) / num_ticks

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.15)

    ticks = np.arange(min_error, max_error, increment)
    cbar = fig.colorbar(im, cax=cax, ticks=ticks)
    cbar.ax.set_yticklabels(["{:.3f}".format(t) for t in ticks])

    cbar.set_label('error value', fontsize=fig_label_size)
    cbar.set_clim(min_error, max_error)

    return fig


def test_pib_dib(pib_net, dib_net, farm,
                 wind_directions=[0, 45, 90],
                 wind_speed=12.0,
                 title_size=15.0,
                 tick_size=11.0,
                 label_size=12.0,
                 annote_turbine=False,
                 annote_power=False,
                 annote_attention=False,
                 show_influential_region=True):

    x_limit = farm.x_grid_size
    y_limit = farm.y_grid_size

    # Parameters for visualization
    fig_size_multipler = 5.0
    viz_eps = 0.05
    scatter_ball_size = x_limit / np.sqrt(120)
    scatter_line_width = scatter_ball_size / 100

    margin_r = 0.6 * (scatter_line_width + scatter_ball_size)
    width_multiplier = scatter_ball_size / 5

    influential_region_zorder = 1
    edge_zorder = 2
    turbine_zorder = 3

    # Compass related parameters
    dir_mark_margin = 0.01 * x_limit
    dir_mark_len = 0.1 * x_limit
    arrow_size = dir_mark_margin

    num_fig = len(wind_directions)

    # color map for power generations
    cmap = matplotlib.cm.Greys
    norm = matplotlib.colors.Normalize(vmin=0.0, vmax=1.0)

    # color map for PIB weights
    attn_cmap = matplotlib.cm.Blues
    attn_norm = matplotlib.colors.Normalize(vmin=0, vmax=1.0)
    scalar_map = matplotlib.cm.ScalarMappable(norm=attn_norm, cmap=attn_cmap)

    # color map for DIB weights
    dib_attn_cmap = matplotlib.cm.Blues
    dib_attn_norm = matplotlib.colors.Normalize(vmin=0, vmax=1.0)
    dib_scalar_map = matplotlib.cm.ScalarMappable(norm=dib_attn_norm, cmap=dib_attn_cmap)

    fig, axes = plt.subplots(num_fig, 2,
                             dpi=300,
                             figsize=(2 * fig_size_multipler, num_fig * fig_size_multipler))
    axes = axes.reshape(-1, 2)
    for i, wd in enumerate(wind_directions):  # row index
        farm.update_wind_farm_graph(wind_speed=wind_speed, wind_direction=wd)

        xs = []
        ys = []
        ps = []

        # Preparing turbine x, y positions and corresponding power, colors
        for node in farm.nodeHelpers:
            xs.append(node.x)
            ys.append(node.y)
            power = farm.graph.nodes[node.index]['power']
            ps.append(power)

        x, y = farm.observe()
        for j in range(axes.shape[1]):  # col index
            ax = axes[i][j]

            ax.set_xlim([-x_limit * viz_eps, x_limit * (1 + viz_eps)])
            ax.set_ylim([-y_limit * viz_eps, y_limit * (1 + viz_eps)])
            ax.xaxis.grid(True, which='major')
            ax.xaxis.grid(True, which='minor')
            ax.yaxis.grid(True, which='major')
            ax.yaxis.grid(True, which='minor')
            ax.tick_params(labelsize=tick_size)

            # Turbine power viz
            turbine_scatter = ax.scatter(xs, ys,
                                         label='Turbine',
                                         cmap=cmap,
                                         c=norm(ps),
                                         vmin=0.0,
                                         vmax=1.0,
                                         edgecolors='#bbbbbb',
                                         linewidths=scatter_line_width,
                                         s=scatter_ball_size,
                                         zorder=turbine_zorder)

            # Turbine index annotations
            for node in farm.nodeHelpers:
                _x = node.x
                _y = node.y

                if annote_turbine:
                    ax.annotate("T{}".format(node.index),
                                xy=(_x, _y + (scatter_line_width + scatter_ball_size) * 1.05),
                                fontsize=11,
                                horizontalalignment="center")
                if annote_power:
                    power = farm.graph.nodes[node.index]['power']
                    ax.annotate("Power {:.3f}".format(power),
                                xy=(_x, _y),
                                fontsize=11,
                                horizontalalignment="center")

                if show_influential_region:
                    wedge = Wedge(
                        (_x, _y),
                        farm.min_distance * 100,
                        -wd - farm.angle_threshold,  # from theta 1 (in degrees)
                        -wd + farm.angle_threshold,  # to theta 2 (in degrees)
                        color='g', alpha=0.05,
                        zorder=influential_region_zorder)
                    ax.add_patch(wedge)

            if j == 0:  # Physics-induced bias
                attention = pib_net.get_attention([x], layer_index=2)[0]  # graph

            else:  # Data-induced bias
                attention = dib_net.get_attention([x], layer_index=2)[0]

            # find max_val for scaling
            max_val = 0
            for e in attention.edges:
                attn_val = attention.edges[e]['attention'].detach().numpy()[0]
                if attn_val >= max_val:
                    max_val = attn_val
                attention.edges[e]['attention'] = attn_val

            # scaling
            for e in attention.edges:
                attn_val = attention.edges[e]['attention']
                attn_val = attn_val / (max_val + 1e-10)
                attention.edges[e]['attention'] = attn_val

            # Draw edges
            for sender in farm.nodeHelpers:
                si = sender.index
                sx = sender.x
                sy = sender.y
                for receiver in sender.interactions:
                    ri = receiver.index
                    rx = receiver.x
                    ry = receiver.y

                    # Adjust sender, receiver position for better visualization
                    # to have marin amount of r
                    dx = rx - sx
                    dy = ry - sy

                    adj_sx = margin_r * dx / np.sqrt(dx * dx + dy * dy)
                    adj_sy = margin_r * dy / np.sqrt(dx * dx + dy * dy)
                    adj_sx = sx + adj_sx
                    adj_sy = sy + adj_sy

                    adj_rx = margin_r * dx / np.sqrt(dx * dx + dy * dy)
                    adj_ry = margin_r * dy / np.sqrt(dx * dx + dy * dy)
                    adj_rx = rx - adj_rx
                    adj_ry = ry - adj_ry

                    adj_dx = adj_rx - adj_sx
                    adj_dy = adj_ry - adj_sy

                    attn_val = attention.edges[(si, ri)]['attention']
                    adj_width = attn_val * width_multiplier  # 0 <= adj_widt <= 1 * width_multiplier
                    if adj_width <= 0.4 * width_multiplier:
                        adj_width = 0.4 * width_multiplier

                    if j == 0:
                        facecolor = scalar_map.to_rgba(attn_val)
                    else:
                        facecolor = dib_scalar_map.to_rgba(attn_val)

                    if attn_val <= 0.0:  # when too small attention values
                        linestyle = 'dashed'
                        adj_width = 0.8 * width_multiplier
                        alpha = 1.0
                        facecolor = 'whitesmoke'
                        edgecolor = 'grey'
                        zorder = edge_zorder - 1.0
                    else:
                        linestyle = 'None'
                        edgecolor = facecolor
                        zorder = edge_zorder
                        alpha = 1.0

                    edge = FancyArrow(adj_sx, adj_sy,
                                      adj_dx, adj_dy,
                                      length_includes_head=True,
                                      width=adj_width,
                                      alpha=alpha,
                                      facecolor=facecolor,
                                      edgecolor=edgecolor,
                                      linestyle=linestyle,
                                      zorder=zorder)

                    ax.add_patch(edge)

                    mid_x = adj_sx + adj_dx / 2
                    mid_y = adj_sy + adj_dy / 2
                    if annote_attention:
                        rotation_angle = np.rad2deg(np.arctan(dy/dx))

                        if attn_val >= 0.001:
                            if j == 0:
                                _s = "{:.3f}".format(attn_val * max_val)
                            else:
                                _s = "{:3.1f}".format(attn_val * max_val)
                            attn_annote = ax.annotate(_s,
                                                      xy=(mid_x, mid_y),
                                                      rotation=rotation_angle,
                                                      color='k',
                                                      fontsize=6,
                                                      horizontalalignment="center",
                                                      zorder=edge_zorder + 3.0)

                            attn_annote.set_path_effects([PathEffects.withStroke(linewidth=5, foreground='w')])

            top_cond = (i == 0)
            bottom_cond = (i == axes.shape[0] - 1)
            left_most_cond = (j == 0)
            right_most_cond = (j == axes.shape[1] - 1)

            if top_cond and left_most_cond:
                ax.set_title("Physics-induced Weight".format(i, j), fontsize=title_size)

                # Attach legend
                handles, labels = ax.get_legend_handles_labels()
                handles += [edge]
                labels += ['weight']
                if show_influential_region:
                    handles += [wedge]
                    labels += ['influential region']

                legend = ax.legend(handles, labels, loc=1)
                legend.legendHandles[0]._sizes = [scatter_ball_size / 2]
                legend.legendHandles[0]._facecolors = [(0.0, 0.0, 0.0)]
                legend.legendHandles[1]._width = [scatter_ball_size / 8]

            if top_cond and right_most_cond:
                ax.set_title("Data-induced Weight".format(i, j), fontsize=title_size)

            if left_most_cond and bottom_cond:
                ax.set_xlabel("Wind farm X size (m)", fontsize=label_size)
                ax.set_ylabel("Wind farm Y size (m)", fontsize=label_size)

            if right_most_cond:

                # Draw directional mark
                dir_mark_center_x = x_limit - dir_mark_margin - dir_mark_len / 2
                dir_mark_center_y = y_limit - dir_mark_margin - dir_mark_len / 2
                marker_len = dir_mark_len / 2

                # WEST
                dir_mark_west_x = dir_mark_center_x - marker_len
                dir_mark_west_y = dir_mark_center_y

                # EAST
                dir_mark_east_x = dir_mark_center_x + marker_len
                dir_mark_east_y = dir_mark_center_y

                # NORTH
                dir_mark_north_x = dir_mark_center_x
                dir_mark_north_y = dir_mark_center_y + marker_len

                # SOUTH
                dir_mark_south_x = dir_mark_center_x
                dir_mark_south_y = dir_mark_center_y - marker_len

                # Visualize wind direction
                compass_color = 'k'
                wd_color = 'orange'

                # WEST -> EAST
                w2e = Line2D((dir_mark_west_x, dir_mark_east_x),
                             (dir_mark_west_y, dir_mark_east_y),
                             alpha=0.5, c=compass_color)
                ax.add_line(w2e)

                # NORTH -> SOUTH
                n2s = Line2D((dir_mark_north_x, dir_mark_south_x),
                             (dir_mark_north_y, dir_mark_south_y),
                             alpha=0.5, c=compass_color)
                ax.add_line(n2s)

                # NORTH -> WEST
                n2w = Line2D((dir_mark_north_x, dir_mark_west_x),
                             (dir_mark_north_y, dir_mark_west_y),
                             alpha=0.5, c=compass_color)
                ax.add_line(n2w)

                # draw wind direction arrow
                wind_dir_rad = np.radians(wd)
                sin = np.sin(wind_dir_rad)
                cos = np.cos(wind_dir_rad)
                wind_start_x = dir_mark_center_x - marker_len * cos  # tail
                wind_start_y = dir_mark_center_y + marker_len * sin

                wind_end_x = dir_mark_center_x + marker_len * cos  # arrow
                wind_end_y = dir_mark_center_y - marker_len * sin

                ax.arrow(wind_start_x, wind_start_y,
                         wind_end_x - wind_start_x, wind_end_y - wind_start_y,
                         linewidth=2,
                         head_width=arrow_size, head_length=arrow_size,
                         fc=wd_color, ec=wd_color, length_includes_head=True,
                         zorder=edge_zorder)

                ax.annotate("Wind direction : {}$^\circ$".format(wd),
                            (dir_mark_west_x - marker_len * 12.0, dir_mark_center_y + marker_len * 0.7),
                            fontsize=11)

                ax.annotate("Wind speed : {} m/s".format(wind_speed),
                            (dir_mark_west_x - marker_len * 12.0, dir_mark_center_y - marker_len * 0.7),
                            fontsize=11)

    fig.subplots_adjust(right=0.9)
    cbar_ax = fig.add_axes([0.95, 0.15, 0.02, 0.7])

    cbar = fig.colorbar(turbine_scatter,
                        cax=cbar_ax,
                        ticks=np.arange(0.0, 1.1, 0.1))
    cbar.set_label('Power', fontsize=label_size)
    cbar.set_clim(0.0, 1.0)
    cbar.ax.tick_params(labelsize=tick_size)

    return fig


def test_pib_dib_colunm_wind_direction(pib_net, dib_net, farm,
                                       wind_directions=[0, 45, 90],
                                       wind_speed=12.0,
                                       title_size=15.0,
                                       tick_size=11.0,
                                       label_size=12.0,
                                       annote_x_label=True,
                                       annote_turbine=False,
                                       annote_power=False,
                                       annote_attention=False,
                                       show_influential_region=True):

    x_limit = farm.x_grid_size
    y_limit = farm.y_grid_size

    # Parameters for visualization
    fig_size_multipler = 5.0
    viz_eps = 0.05
    scatter_ball_size = x_limit / np.sqrt(120)
    scatter_line_width = scatter_ball_size / 100

    margin_r = 0.6 * (scatter_line_width + scatter_ball_size)
    width_multiplier = scatter_ball_size / 5

    influential_region_zorder = 1
    edge_zorder = 2
    turbine_zorder = 3

    # Compass related parameters
    dir_mark_margin = 0.01 * x_limit
    dir_mark_len = 0.1 * x_limit
    arrow_size = dir_mark_margin

    num_fig = len(wind_directions)

    # color map for power generations
    cmap = matplotlib.cm.Greys
    norm = matplotlib.colors.Normalize(vmin=0.0, vmax=1.0)

    # color map for PIB weights
    attn_cmap = matplotlib.cm.Blues
    attn_norm = matplotlib.colors.Normalize(vmin=0, vmax=1.0)
    scalar_map = matplotlib.cm.ScalarMappable(norm=attn_norm, cmap=attn_cmap)

    fig, axes = plt.subplots(2, num_fig,
                             dpi=300,
                             figsize=(num_fig * fig_size_multipler, 2 * fig_size_multipler))
    axes = axes.reshape(2, -1)
    for i in range(axes.shape[0]):  # row index
        for j, wd in enumerate(wind_directions):
            farm.update_wind_farm_graph(wind_speed=wind_speed, wind_direction=wd)

            xs = []
            ys = []
            ps = []

            # Preparing turbine x, y positions and corresponding power, colors
            for node in farm.nodeHelpers:
                xs.append(node.x)
                ys.append(node.y)
                power = farm.graph.nodes[node.index]['power']
                ps.append(power)

            x, y = farm.observe()

            ax = axes[i][j]

            ax.set_xlim([-x_limit * viz_eps, x_limit * (1 + viz_eps)])
            ax.set_ylim([-y_limit * viz_eps, y_limit * (1 + viz_eps)])
            ax.xaxis.grid(True, which='major')
            ax.xaxis.grid(True, which='minor')
            ax.yaxis.grid(True, which='major')
            ax.yaxis.grid(True, which='minor')
            ax.tick_params(labelsize=tick_size)

            # Turbine power viz
            turbine_scatter = ax.scatter(xs, ys,
                                         label='Turbine',
                                         cmap=cmap,
                                         c=norm(ps),
                                         vmin=0.0,
                                         vmax=1.0,
                                         edgecolors='#bbbbbb',
                                         linewidths=scatter_line_width,
                                         s=scatter_ball_size,
                                         zorder=turbine_zorder)

            # Turbine index annotations
            for node in farm.nodeHelpers:
                _x = node.x
                _y = node.y

                if annote_turbine:
                    ax.annotate("T{}".format(node.index),
                                xy=(_x, _y + (scatter_line_width + scatter_ball_size) * 1.05),
                                fontsize=11,
                                horizontalalignment="center")
                if annote_power:
                    power = farm.graph.nodes[node.index]['power']
                    ax.annotate("Power {:.3f}".format(power),
                                xy=(_x, _y),
                                fontsize=11,
                                horizontalalignment="center")

                if show_influential_region:
                    wedge = Wedge(
                        (_x, _y),
                        farm.min_distance * 100,
                        -wd - farm.angle_threshold,  # from theta 1 (in degrees)
                        -wd + farm.angle_threshold,  # to theta 2 (in degrees)
                        color='g', alpha=0.05,
                        zorder=influential_region_zorder)
                    ax.add_patch(wedge)

            if i == 0:  # Physics-induced bias
                attention = pib_net.get_attention([x], layer_index=2)[0]  # graph

            else:  # Data-induced bias
                attention = dib_net.get_attention([x], layer_index=2)[0]

            # find max_val for scaling
            max_val = 0
            for e in attention.edges:
                attn_val = attention.edges[e]['attention'].detach().numpy()[0]
                if attn_val >= max_val:
                    max_val = attn_val
                attention.edges[e]['attention'] = attn_val

            # scaling
            for e in attention.edges:
                attn_val = attention.edges[e]['attention']
                attn_val = attn_val / (max_val + 1e-10)
                attention.edges[e]['attention'] = attn_val

            # Draw edges
            for sender in farm.nodeHelpers:
                si = sender.index
                sx = sender.x
                sy = sender.y
                for receiver in sender.interactions:
                    ri = receiver.index
                    rx = receiver.x
                    ry = receiver.y

                    # Adjust sender, receiver position for better visualization
                    # to have marin amount of r
                    dx = rx - sx
                    dy = ry - sy

                    adj_sx = margin_r * dx / np.sqrt(dx * dx + dy * dy)
                    adj_sy = margin_r * dy / np.sqrt(dx * dx + dy * dy)
                    adj_sx = sx + adj_sx
                    adj_sy = sy + adj_sy

                    adj_rx = margin_r * dx / np.sqrt(dx * dx + dy * dy)
                    adj_ry = margin_r * dy / np.sqrt(dx * dx + dy * dy)
                    adj_rx = rx - adj_rx
                    adj_ry = ry - adj_ry

                    adj_dx = adj_rx - adj_sx
                    adj_dy = adj_ry - adj_sy

                    attn_val = attention.edges[(si, ri)]['attention']
                    adj_width = attn_val * width_multiplier  # 0 <= adj_widt <= 1 * width_multiplier
                    if adj_width <= 0.4 * width_multiplier:
                        adj_width = 0.4 * width_multiplier

                    facecolor = scalar_map.to_rgba(attn_val)

                    if attn_val <= 0.0:  # when too small attention values
                        linestyle = 'dashed'
                        adj_width = 0.8 * width_multiplier
                        alpha = 1.0
                        facecolor = 'whitesmoke'
                        edgecolor = 'grey'
                        zorder = edge_zorder - 1.0
                    else:
                        linestyle = 'None'
                        edgecolor = facecolor
                        zorder = edge_zorder
                        alpha = 1.0

                    edge = FancyArrow(adj_sx, adj_sy,
                                      adj_dx, adj_dy,
                                      length_includes_head=True,
                                      width=adj_width,
                                      alpha=alpha,
                                      facecolor=facecolor,
                                      edgecolor=edgecolor,
                                      linestyle=linestyle,
                                      zorder=zorder)

                    ax.add_patch(edge)

                    mid_x = adj_sx + adj_dx / 2
                    mid_y = adj_sy + adj_dy / 2

                    if annote_attention:
                        rotation_angle = np.rad2deg(np.arctan(dy/dx))

                        if attn_val >= 0.001:
                            if j == 0:
                                _s = "{:.3f}".format(attn_val * max_val)
                            else:
                                _s = "{:3.1f}".format(attn_val * max_val)
                            attn_annote = ax.annotate(_s,
                                                      xy=(mid_x, mid_y),
                                                      rotation=rotation_angle,
                                                      color='k',
                                                      fontsize=6,
                                                      horizontalalignment="center",
                                                      zorder=edge_zorder + 3.0)

                            attn_annote.set_path_effects([PathEffects.withStroke(linewidth=5, foreground='w')])

            top_cond = (i == 0)
            bottom_cond = (i == axes.shape[0] - 1)
            left_most_cond = (j == 0)
            right_most_cond = (j == axes.shape[1] - 1)

            if top_cond:
                if annote_x_label:
                    ax.set_title("Wind direction : {}$^\circ$".format(wd), fontsize=title_size)

            if top_cond and left_most_cond:
                ax.set_ylabel("Physics-induced Weight".format(i, j), fontsize=title_size)

                # # Attach legend
                # handles, labels = ax.get_legend_handles_labels()
                # handles += [edge]
                # labels += ['weight']
                # if show_influential_region:
                #     handles += [wedge]
                #     labels += ['influential region']
                #
                # legend = ax.legend(handles, labels, loc=1)
                # legend.legendHandles[0]._sizes = [scatter_ball_size / 2]
                # legend.legendHandles[0]._facecolors = [(0.0, 0.0, 0.0)]
                # legend.legendHandles[1]._width = [scatter_ball_size / 8]

            if bottom_cond and left_most_cond:
                ax.set_ylabel("Data-induced Weight".format(i, j), fontsize=title_size)

            # if left_most_cond and bottom_cond:
            #     ax.set_xlabel("Wind farm X size (m)", fontsize=label_size)
            #     ax.set_ylabel("Wind farm Y size (m)", fontsize=label_size)

            if top_cond:

                # Draw directional mark
                dir_mark_center_x = x_limit - dir_mark_margin - dir_mark_len / 2
                dir_mark_center_y = y_limit - dir_mark_margin - dir_mark_len / 2
                marker_len = dir_mark_len / 2

                # WEST
                dir_mark_west_x = dir_mark_center_x - marker_len
                dir_mark_west_y = dir_mark_center_y

                # EAST
                dir_mark_east_x = dir_mark_center_x + marker_len
                dir_mark_east_y = dir_mark_center_y

                # NORTH
                dir_mark_north_x = dir_mark_center_x
                dir_mark_north_y = dir_mark_center_y + marker_len

                # SOUTH
                dir_mark_south_x = dir_mark_center_x
                dir_mark_south_y = dir_mark_center_y - marker_len

                # Visualize wind direction
                compass_color = 'k'
                wd_color = 'orange'

                # WEST -> EAST
                w2e = Line2D((dir_mark_west_x, dir_mark_east_x),
                             (dir_mark_west_y, dir_mark_east_y),
                             alpha=0.5, c=compass_color)
                ax.add_line(w2e)

                # NORTH -> SOUTH
                n2s = Line2D((dir_mark_north_x, dir_mark_south_x),
                             (dir_mark_north_y, dir_mark_south_y),
                             alpha=0.5, c=compass_color)
                ax.add_line(n2s)

                # NORTH -> WEST
                n2w = Line2D((dir_mark_north_x, dir_mark_west_x),
                             (dir_mark_north_y, dir_mark_west_y),
                             alpha=0.5, c=compass_color)
                ax.add_line(n2w)

                # draw wind direction arrow
                wind_dir_rad = np.radians(wd)
                sin = np.sin(wind_dir_rad)
                cos = np.cos(wind_dir_rad)
                wind_start_x = dir_mark_center_x - marker_len * cos  # tail
                wind_start_y = dir_mark_center_y + marker_len * sin

                wind_end_x = dir_mark_center_x + marker_len * cos  # arrow
                wind_end_y = dir_mark_center_y - marker_len * sin

                ax.arrow(wind_start_x, wind_start_y,
                         wind_end_x - wind_start_x, wind_end_y - wind_start_y,
                         linewidth=2,
                         head_width=arrow_size, head_length=arrow_size,
                         fc=wd_color, ec=wd_color, length_includes_head=True,
                         zorder=edge_zorder)

                ax.annotate("Wind direction : {}$^\circ$".format(wd),
                            (dir_mark_west_x - marker_len * 12.0, dir_mark_center_y + marker_len * 0.7),
                            fontsize=11)

                ax.annotate("Wind speed : {} m/s".format(wind_speed),
                            (dir_mark_west_x - marker_len * 12.0, dir_mark_center_y - marker_len * 0.7),
                            fontsize=11)

    fig.subplots_adjust(right=0.95)
    cbar_ax = fig.add_axes([0.98, 0.15, 0.02, 0.7])

    cbar = fig.colorbar(turbine_scatter,
                        cax=cbar_ax,
                        ticks=np.arange(0.0, 1.1, 0.1))
    cbar.set_label('Power', fontsize=label_size)
    cbar.set_clim(0.0, 1.0)
    cbar.ax.tick_params(labelsize=tick_size)

    return fig