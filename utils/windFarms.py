import json

import networkx as nx
import numpy as np
from floris.floris import Floris
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, FancyArrow
from matplotlib.lines import Line2D
import matplotlib
from matplotlib import cm

from utils.windFarmUtils import prepare_floris_json, valid_layout, sequential_sampling
from utils.nodeManagingHelpers import NodeHelper
from utils.timeout import timeout


DEBUG = True
SINGLE_INPUT_PATH = './configs/example_input_single.json'
SINGLE_INPUT_INDEX = 0  # A hacky way for passing keyerror


class BaseWindFarmManager:

    def __init__(self,
                 x_grid_size,
                 y_grid_size,
                 base_config_json_path,
                 update_config_json_path,
                 single_input_path,
                 min_distance_factor=5.0,
                 **kwargs):

        self.base_config_json_path = base_config_json_path
        self.update_config_json_path = update_config_json_path
        self.single_input_path = single_input_path
        self.min_distance_factor = min_distance_factor
        self.x_grid_size = x_grid_size
        self.y_grid_size = y_grid_size

        base_config = self._load_json(base_config_json_path)
        diameter = base_config['turbine']['properties']['rotor_diameter']
        self.turbine_diameter = diameter
        self.min_distance = min_distance_factor * diameter

        self.floris_obj = None
        self.x_coords = None
        self.y_coords = None

        # Properties for constructing graph
        self.graph = nx.DiGraph()

        # Properties for indexing turbines and influences
        self.coords2index = {}
        self.index2coords = {}

    def _update_wind_farm_floris(self, x_coords, y_coords):
        """
        :param x_coords: (list of floats) a list contains x coordinates of wind turbines
        :param y_coords: (list of floats) a list contains y coordinates of wind turbines
        :return: None
        """
        _ = prepare_floris_json(template_path=self.base_config_json_path,
                                xy_layouts=[x_coords, y_coords],
                                save_path=self.update_config_json_path,
                                save=True)

        self.floris_obj = Floris(self.update_config_json_path)

    @staticmethod
    def _load_json(json_path):
        with open(json_path) as f:
            data = json.load(f)
        return data


class RandomSampleManager(BaseWindFarmManager):

    def __init__(self,
                 x_grid_size,
                 y_grid_size,
                 update_config_json_path,
                 base_config_json_path,
                 single_input_path,
                 angle_threshold=90,
                 min_distance_factor=5.0,
                 dist_cutoff_factor=50.0):

        super(RandomSampleManager, self).__init__(x_grid_size=x_grid_size,
                                                  y_grid_size=y_grid_size,
                                                  update_config_json_path=update_config_json_path,
                                                  base_config_json_path=base_config_json_path,
                                                  single_input_path=single_input_path,
                                                  min_distance_factor=min_distance_factor)

        self.angle_threshold = angle_threshold
        self.dist_cutoff_factor = dist_cutoff_factor

        # Constructed by constructor
        self.dist_threshold = self.turbine_diameter * self.dist_cutoff_factor
        self.nodeHelpers = []

        # Will be determined
        self.num_turbine = None
        self.wind_direction = None
        self.wind_speed = None
        self.x_coords = None
        self.y_coords = None

    @timeout()
    def sample_wind_farm(self,
                         num_turbine,
                         x_grid_size=None,
                         y_grid_size=None):

        # Initialize NodeHelpers
        NodeHelper.initialize_node_helper()
        self.num_turbine = num_turbine

        if x_grid_size is None:
            x_grid_size = self.x_grid_size

        if y_grid_size is None:
            y_grid_size = self.y_grid_size

        sampled_x, sampled_y = sequential_sampling(x_grid_size,
                                                   y_grid_size,
                                                   self.min_distance,
                                                   num_turbine)

        if valid_layout(sampled_x, sampled_y, self.min_distance):
            self._update_wind_farm_floris(sampled_x, sampled_y)
            self.x_coords = sampled_x
            self.y_coords = sampled_y
        else:
            raise RuntimeError("Sampled layout is not valid")

        if self.wind_direction is not None and self.wind_speed is not None:
            self.update_wind_farm_graph(wind_direction=self.wind_direction,
                                        wind_speed=self.wind_speed)

    @classmethod
    def get_input_params(cls):
        input_params = ['x_grid_size', 'y_grid_size',
                        'update_config_json_path', 'base_config_json_path', 'single_input_path',
                        'angle_threshold', 'min_distance_factor', 'dist_cutoff_factor']
        return input_params

    @classmethod
    def get_constructed_params(cls):
        constructed_params = ['x_coords', 'y_coords', 'num_turbine', 'wind_direction', 'wind_speed']
        return constructed_params

    def save(self, path):
        """
        :param path: path for saving current wind farm layout
            ex) path = './farm_saves/standard_farm
        """
        input_params = self.get_input_params()
        cons_params = self.get_constructed_params()

        j = dict()
        for attr, val in self.__dict__.items():
            if attr in input_params or attr in cons_params:
                j[attr] = val
        j['base_config_json_path'] = self.update_config_json_path

        save_path = path + '.json'
        j['save_path'] = save_path

        with open(save_path, 'w') as f:
            json.dump(j, f)

    @classmethod
    def load(cls, path):
        """
        :param path: path for saving current wind farm layout
            ex) path = './farm_saves/standard_farm
        """
        load_path = path + '.json'
        with open(load_path) as f:
            j = json.load(f)

        j_input = dict()
        j_cons = dict()

        input_params = cls.get_input_params()
        cons_params = cls.get_constructed_params()
        for attr, val in j.items():
            if attr in input_params:
                j_input[attr] = val
            if attr in cons_params:
                j_cons[attr] = val

        farm_load = cls(**j_input)

        # Manually set wind farm layouts
        farm_load._update_wind_farm_floris(x_coords=j_cons['x_coords'],
                                           y_coords=j_cons['y_coords'])
        farm_load.x_coords = j_cons['x_coords']
        farm_load.y_coords = j_cons['y_coords']

        # Update wind farm graph depending on the given wind speed and wind direction

        farm_load.update_wind_farm_graph(wind_speed=j_cons['wind_speed'],
                                         wind_direction=j_cons['wind_direction'])
        farm_load.num_turbine = len(j_cons['x_coords'])
        farm_load.wind_speed = j_cons['wind_speed']
        farm_load.wind_direction = j_cons['wind_direction']

        return farm_load

    def _get_power(self, wind_direction, wind_speed, normalize=True):
        def __get_power(floris_obj, wd, ws, scale):
            flow_field = floris_obj.farm.flow_field

            flow_field.wind_direction = np.radians(wd)
            flow_field.wind_speed = ws
            flow_field.initial_flowfield = flow_field._initial_flowfield()
            flow_field.u_field = flow_field._initial_flowfield()
            flow_field.calculate_wake()

            total_power = 0.0
            powers = {}

            coords = [coord for coord, _ in flow_field.turbine_map.items()]
            turbines = [turbine for _, turbine in flow_field.turbine_map.items()]
            for coord, turbine in zip(coords, turbines):
                try:
                    idx = self.coords2index[(coord.x, coord.y)]
                except KeyError:
                    "hacky way for handling single input case"
                    idx = SINGLE_INPUT_INDEX
                p = turbine.power  # single turbine power
                scaled_p = p/scale
                powers[idx] = scaled_p
                total_power += scaled_p

            return powers, total_power

        if normalize:
            single_floris = Floris(self.single_input_path)
            power_single, __ = __get_power(single_floris, wind_direction, wind_speed, 1.0)
            power_single = power_single[SINGLE_INPUT_INDEX]  # get power_single value
            powers, __ = __get_power(self.floris_obj, wind_direction, wind_speed, power_single)
        else:
            powers, __ = __get_power(self.floris_obj, wind_direction, wind_speed, 1.0)
        return powers

    def set_layout(self, x_coords, y_coords, wind_direction=0, wind_speed=12.0):

        if max(x_coords) > self.x_grid_size:
            raise RuntimeError("invalid x-grid layouts")

        if max(y_coords) > self.y_grid_size:
            raise RuntimeError("invalid y-grid layouts")

        # Clear node list
        self.nodeHelpers = []

        # Initialize Graph
        self.graph.clear()  # clear all nodes and edges in graph

        self._update_wind_farm_floris(x_coords=x_coords,
                                      y_coords=y_coords)
        self.x_coords = x_coords
        self.y_coords = y_coords
        self.update_wind_farm_graph(wind_direction=wind_direction,
                                    wind_speed=wind_speed)
        self.num_turbine = len(x_coords)
        self.wind_speed = wind_speed
        self.wind_direction = wind_direction

    def update_wind_farm_graph(self,
                               wind_direction,
                               wind_speed,
                               normalize_power=True,
                               cut_off_angle=None,
                               cut_off_dist=None,
                               verbose=False):

        self.wind_direction = wind_direction
        self.wind_speed = wind_speed

        if cut_off_angle is None:
            cut_off_angle = self.angle_threshold

        if cut_off_dist is None:
            cut_off_dist = self.dist_threshold

        # Clear node list
        self.nodeHelpers = []

        # Initialize Graph
        self.graph.clear()  # clear all nodes and edges in graph

        # Add global properties of the embedded graph
        self.graph.graph['wind_direction'] = wind_direction
        self.graph.graph['wind_speed'] = wind_speed

        # Instantiate influence graph on given wind_direction, cut_off_angle, and cut_off_distance
        coords = zip(self.x_coords, self.y_coords)
        coords2d = np.stack([self.x_coords, self.y_coords]).T

        # Calculate interaction patterns and save the patterns in interactions property.
        for x_ref, y_ref in coords:
            ref_node = NodeHelper.get_helper(x_ref, y_ref, cut_off_angle, cut_off_dist)
            ref_node.clear_interaction()  # Clear NodeHelper's helper properties
            ref_node.add_interactions(wind_direction=wind_direction,
                                      coords=coords2d,
                                      verbose=verbose)
            self.nodeHelpers.append(ref_node)

        # Add turbines as the nodes on the networkx graph object
        coord_turbine_pairs = self.floris_obj.farm.turbine_map.items()
        for coord_turbine in coord_turbine_pairs:

            # Ugly indexing cascaded from 'floris'
            # coord_turbine[0] = coord obj from 'floris'
            # coord_turbine[1] = turbine ojb from 'floris'

            x, y = coord_turbine[0].x, coord_turbine[0].y
            node = NodeHelper.get_helper(x, y)
            if node.built:
                self.coords2index[(x, y)] = node.index
                self.index2coords[node.index] = (x, y)
                self.graph.add_node(node.index, coords=(x, y))
                if node.interactions:  # when the target node has some interaction w/ other turbines
                    for target_node, edge_attr in node.interactions.items():
                        self.graph.add_edge(node.index, target_node.index, **edge_attr)

        index_power_dict = self._get_power(wind_direction,
                                           wind_speed,
                                           normalize=normalize_power)

        for index, power in index_power_dict.items():
            self.graph.node[index]['power'] = power
        if verbose:
            self.visualize_wind_farm(wind_direction, wind_speed)

    def observe(self, use_speed=True, noise_factor=0.00):
        """
        A helper method for observing current status of windfarm
        :return: x_g: networkx graph contain input features WITHOUT power
                 y_g: networkx graph contain power. has no edge connections
        """
        x_g = self.graph.copy()
        y_g = self.graph.copy()

        for n in x_g.nodes:
            del x_g.nodes[n]['power']
            del x_g.nodes[n]['coords']
            x_g.nodes[n]['dummy'] = 0.0
            if use_speed:
                x_g.nodes[n]['speed'] = x_g.graph['wind_speed']
        
        if noise_factor > 0.00:
            num_node = len(y_g.nodes())
            noises = np.random.normal(loc=0.0, scale=noise_factor, size=num_node)
        
            for n, noise in zip(y_g.nodes, noises):
                del y_g.nodes[n]['coords']
                y_g.nodes[n]['power'] += noise
        else:
            for n in y_g.nodes:
                del y_g.nodes[n]['coords']
            
        return x_g, y_g

    def observe_viz(self):
        # Helper function that return state of wind-farm in 'as is' format.
        return nx.DiGraph(self.graph)

    def visualize_wind_farm(self,
                            wind_direction=None,
                            wind_speed=None,
                            dir_mark_margin=0.01,
                            dir_mark_len=0.03,
                            arrow_size=0.01,
                            viz_eps=0.02,
                            dpi=250,
                            normalize_power=True,
                            return_fig=False,
                            label_size=15,
                            tick_size=12,
                            annotation_size=12,
                            show_color_bar_label=False,
                            title_size=20,
                            show_title=False,
                            edge_width=8,
                            legend_size=15):

        influential_region_zorder = 0
        min_distance_region_zorder = 1
        edge_zorder = 2
        turbine_zorder = 3

        scatter_line_width = 2.0
        scatter_ball_size = 200

        if wind_direction is None:
            wind_direction = self.wind_direction

        if wind_speed is None:
            wind_speed = self.wind_speed

        x_limit = self.x_grid_size
        y_limit = self.y_grid_size

        # fig = plt.figure(figsize=(12, 10), dpi=dpi)
        # ax = fig.add_subplot(111)

        fig, (ax, ax_colorbar) = plt.subplots(1, 2,
                                              figsize=(10.5, 10),
                                              gridspec_kw={'width_ratios': [10, 0.5]},
                                              dpi=dpi)
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])

        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(True, which='minor')
        ax.yaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='minor')
        ax.set_xlim([-x_limit * viz_eps, x_limit * (1 + viz_eps)])
        ax.set_ylim([-y_limit * viz_eps, y_limit * (1 + viz_eps)])
        ax.tick_params(labelsize=tick_size)

        cmap = matplotlib.cm.Blues
        norm = matplotlib.colors.Normalize(vmin=0.0, vmax=1.0)

        index_power_dict = self._get_power(wind_direction=wind_direction,
                                           wind_speed=wind_speed,
                                           normalize=normalize_power)
        xs = []
        ys = []
        cs = []
        ps = []
        for node in self.nodeHelpers:
            xs.append(node.x)
            ys.append(node.y)
            power = index_power_dict[node.index]
            ps.append(power)
            r, g, b, a = cmap(norm(power))
            c = np.array([r, g, b, a]).reshape(1, -1)
            cs.append(c)

        turbine_scatter = ax.scatter(xs, ys,
                                     label='Turbine',
                                     cmap=cmap,
                                     c=norm(ps),
                                     edgecolors='#bbbbbb',
                                     linewidths=scatter_line_width,
                                     s=scatter_ball_size,
                                     zorder=turbine_zorder)

        ax.set_xlabel("Wind farm X size (m)", fontsize=label_size)
        ax.set_ylabel("Wind farm Y size (m)", fontsize=label_size)

        cbar = fig.colorbar(turbine_scatter,
                            cax=ax_colorbar,
                            ticks=np.arange(0.0, 1.1, 0.05))

        if show_color_bar_label:
            cbar.set_label('Power', fontsize=label_size)
        cbar.set_clim(0.0, 1.0)
        cbar.ax.tick_params(labelsize=tick_size)

        for x, y in zip(self.x_coords, self.y_coords):
            # Add min-distance circle
            circle = plt.Circle((x, y),
                                self.min_distance/2,
                                hatch='....',
                                facecolor='white',
                                edgecolor='#ffa45c',
                                alpha=1.0,
                                zorder=min_distance_region_zorder)
            node = NodeHelper.get_helper(x, y)
            if not node.built:
                raise RuntimeError("Access to not instantiated nodeHelper")

            # Add annotation for depicting turbine index and the corresponding power
            ax.annotate("T{}".format(node.index+1),
                        xy=(x, y+y_limit*0.025),
                        fontsize=annotation_size,
                        horizontalalignment="center")
            # ax.annotate("Power : {:.2f}".format(index_power_dict[node.index]),
            #             (x, y - y_limit * viz_eps))
            ax.add_artist(circle)

            # Add influential cones
            wedge = Wedge(
                (x, y),
                self.min_distance * 100,  # radius
                -wind_direction - self.angle_threshold,  # from theta 1 (in degrees)
                -wind_direction + self.angle_threshold,  # to theta 2 (in degrees)
                color='g', alpha=0.05,
                zorder=influential_region_zorder)
            ax.add_patch(wedge)

        # Draw directional mark
        dir_mark_center_x = x_limit * (1 - 2 * dir_mark_margin - dir_mark_len)
        dir_mark_center_y = y_limit * (1 - 2 * dir_mark_margin - dir_mark_len)

        marker_len = x_limit * dir_mark_len

        # WEST
        dir_mark_west_x = dir_mark_center_x - marker_len
        dir_mark_west_y = dir_mark_center_y

        # EAST
        dir_mark_east_x = dir_mark_center_x + marker_len
        dir_mark_east_y = dir_mark_center_y

        # NORTH
        dir_mark_north_x = dir_mark_center_x
        dir_mark_north_y = dir_mark_center_y + marker_len

        # SOUTH
        dir_mark_south_x = dir_mark_center_x
        dir_mark_south_y = dir_mark_center_y - marker_len

        # Visualize wind direction

        # Compass
        compass_color = 'k'
        wd_color = 'orange'
        arrow_size = x_limit * arrow_size

        # WEST -> EAST
        w2e = Line2D((dir_mark_west_x, dir_mark_east_x),
                     (dir_mark_west_y, dir_mark_east_y),
                     alpha=0.5, c=compass_color)

        ax.add_line(w2e)

        # NORTH -> SOUTH
        n2s = Line2D((dir_mark_north_x, dir_mark_south_x),
                     (dir_mark_north_y, dir_mark_south_y),
                     alpha=0.5, c=compass_color)

        ax.add_line(n2s)

        # NORTH -> WEST
        n2w = Line2D((dir_mark_north_x, dir_mark_west_x),
                     (dir_mark_north_y, dir_mark_west_y),
                     alpha=0.5, c=compass_color)

        ax.add_line(n2w)

        # draw wind direction arrow
        wind_dir_rad = np.radians(wind_direction)
        sin = np.sin(wind_dir_rad)
        cos = np.cos(wind_dir_rad)
        wind_start_x = dir_mark_center_x - marker_len * cos  # tail
        wind_start_y = dir_mark_center_y + marker_len * sin

        wind_end_x = dir_mark_center_x + marker_len * cos  # arrow
        wind_end_y = dir_mark_center_y - marker_len * sin

        ax.arrow(wind_start_x, wind_start_y,
                 wind_end_x - wind_start_x, wind_end_y - wind_start_y,
                 linewidth=2,
                 head_width=arrow_size, head_length=arrow_size,
                 fc=wd_color, ec=wd_color, length_includes_head=True,
                 zorder=edge_zorder)

        ax.annotate("Wind direction : {}$^\circ$".format(wind_direction),
                    (dir_mark_west_x - marker_len * 7.0,
                     dir_mark_center_y + marker_len * 0.5))

        ax.annotate("Wind speed : {} m/s".format(wind_speed),
                    (dir_mark_west_x - marker_len * 7.0,
                     dir_mark_center_y - marker_len * 0.5))

        handles, labels = ax.get_legend_handles_labels()
        handles += [wedge, circle]
        labels += ["influential region", "min. distance"]
        ax.legend(handles, labels, prop={'size': legend_size})

        # Draw interaction graph
        margin_r = 15
        edges = []
        for sender in self.nodeHelpers:
            sx = sender.x
            sy = sender.y
            for receiver in sender.interactions:
                rx = receiver.x
                ry = receiver.y

                # Adjust sender, receiver position for better visualization
                # to have marin amount of r
                dx = rx - sx
                dy = ry - sy

                adj_sx = margin_r * dx / np.sqrt(dx * dx + dy * dy)
                adj_sy = margin_r * dy / np.sqrt(dx * dx + dy * dy)
                adj_sx = sx + adj_sx
                adj_sy = sy + adj_sy

                adj_rx = margin_r * dx / np.sqrt(dx * dx + dy * dy)
                adj_ry = margin_r * dy / np.sqrt(dx * dx + dy * dy)
                adj_rx = rx - adj_rx
                adj_ry = ry - adj_ry

                adj_dx = adj_rx - adj_sx
                adj_dy = adj_ry - adj_sy

                edge = FancyArrow(adj_sx, adj_sy,
                                  adj_dx, adj_dy,
                                  length_includes_head=True,
                                  width=edge_width,
                                  color='#bbbbbb',  # #bbbbbb
                                  zorder=edge_zorder)
                ax.add_patch(edge)
                edges.append(edge)

        if show_title:
            fig.suptitle('Wind Farm Layout', fontsize=title_size)

        if return_fig:
            return fig, ax
